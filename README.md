# dotfiles

Personal Unix configuration files.

The repository works by storing the configuration files and then using [GNU Stow](https://www.gnu.org/software/stow/) to create/manage symbolic links so that configuration files used reference the ones in this repository.

Inspirations:
- [xero/dotfiles](https://github.com/xero/dotfiles)

## Installation

These configurations are intended for use on Linux-based operating systems (including those run under Windows using Windows Subsystem for Linux).

You will need [GNU Stow](https://www.gnu.org/software/stow/) installed.

Clone this repository into your home directory.

    jtarka@example:~$: git clone git@gitlab.com:jamestarka/dotfiles.git

It is possible to use this repository outside of your home directory, but additional configuration (not described here) will be needed for Stow to work correctly.

## Usage

All of the commands below should be executed from the root directory of this repository.

    $ cd ~/dotfiles

The available configurations are the top-level directories within this repository. The examples show management of the ``git`` configuration.

**Install a configuration**

    $ stow [configuration-name]
    $ stow git

If there are files that Stow would replace, you will be prompted to remove them before the Stow configuration can be installed.

**Uninstall a configuration**

    $ stow -D [configuration-name]
    $ stow -D git

**Update a configuration**

Stow works using symbolic links. This means that any changes to an exisitng file in an installed configuration will automatically be reflected in the "live"/installed files.

However, adding a new file to a configuration will require you to install the configuration again.

    $ stow [configuration-name]

**Create a new configuration**

Create a top-level directory for your application. Add any "dotfiles" needed to that directory. Then follow the steps above to install/uninstall them as desired.

# License

Since dotfiles are a personal preference, I doubt that you would want to exactly copy or directly use the contents of this repository. Feel free to copy its structure or any configuration values that I use.

If you would like this project to have a license, please open an issue with your rationale.